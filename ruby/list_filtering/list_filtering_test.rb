require 'minitest/autorun'
require_relative 'list_filtering'

class ListFilteringTest < Minitest::Test
    def test_count_one_number
        assert_equal [1], filter_list([1])
    end

    def test_count_2_number
        assert_equal [1,2], filter_list([1,2])
    end  

    def test_with_one_char
        assert_equal [10,20], filter_list([10,'b',20])
    end

    def test_with_2_char_like_number
        assert_equal [2,4,3], filter_list(['1',2,'3',4,3])
    end

    def test_empty
        assert_equal [], filter_list([])
    end
end    